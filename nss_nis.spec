Name: nss_nis
Version: 3.2
Release: 2
Summary: Name Service Switch (NSS) module using NIS
License: LGPL-2.1-or-later
URL: https://github.com/thkukuk/libnss_nis
Source: https://github.com/thkukuk/libnss_nis/archive/v%{version}.tar.gz
# https://github.com/systemd/systemd/issues/7074
# https://bugzilla.redhat.com/show_bug.cgi?id=1829572
Source2: nss_nis.conf
BuildRequires: autoconf automake libtool
BuildRequires: pkgconfig(libnsl)
BuildRequires: pkgconfig(libtirpc)
BuildRequires: rpm_macro(_unitdir)

%description
This package contains the NSS NIS plugin for glibc. This code was formerly
part of glibc, but is now standalone to be able to link against TI-RPC for
IPv6 support. The nss_nis Name Service Switch module uses the Network
Information System (NIS) to obtain user, group, host name, and other data.

%package devel
Summary:  Static library for nss_nis
Requires: %{name} = %{version}-%{release}

%description devel
Static library for nss_nis.

%prep
%autosetup -n lib%{name}-%{version}

%build
autoreconf -fiv
%configure --disable-static
%make_build

%install
%make_install
%delete_la

install -D -m 644 %{S:2} %{buildroot}/%{_unitdir}/systemd-logind.service.d/nss_nis.conf
install -D -m 644 %{S:2} %{buildroot}/%{_unitdir}/systemd-userdbd.service.d/nss_nis.conf

%check
%make_build check

%files
%license COPYING
%{_libdir}/lib%{name}.so.2
%{_libdir}/lib%{name}.so.2.*
%{_unitdir}/systemd-logind.service.d/*
%{_unitdir}/systemd-userdbd.service.d/*

%files devel
%{_libdir}/lib%{name}.so

%changelog
* Wed Oct 09 2024 Funda Wang <fundawang@yeah.net> - 3.2-2
- cleanup spec
- Add systemd-logind snippet (IPAddressAllow=any)

* Tue Jul 18 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 3.2-1
- Update to 3.2

* Mon Jul 27 2020 Liquor <lirui130@huawei.com> - 3.1-1
- update to 3.1

* Mon Sep 2 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.0.8
- Package init

